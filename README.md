# pipes-map

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Notes
1. Added three buttons (Year, Diameter, Material) which when pressed will update color of pipes based on year, diameter, and material type of pipes.  Pipes are given shades of red color based on year, shades of blue color based on diameter, and shades of green color based on material type.  All the legends for these colors displays at the bottom right of the map.
2. Added Update Pipe button which when pressed will update a random pipe to random year between 1960 and 2020.  When this button is pressed a message will display in red color indicating which pipe is updated and to which year.  The message disappears when the Year, Diameter, or Material buttons pressed.